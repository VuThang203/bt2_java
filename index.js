// Cau1:
function tinhTienLuong(){
    event.preventDefault();
    var luongMotNgay = 100.000;
    var ngayLam = document.getElementById("txt-ngay-lam").value*1;
    var tienLuong = (luongMotNgay*ngayLam);
    document.getElementById("result").innerHTML = `Tiền lương phải trả ${tienLuong.toFixed(3)}VND`
}

// cau 2:
function tinhTrungBinh(){
    event.preventDefault();
    var soThuNhat = document.getElementById("txt-so-mot").value*1;
    var soThuHai = document.getElementById("txt-so-hai").value*1;
    var soThuBa = document.getElementById("txt-so-ba").value*1;
    var soThuBon = document.getElementById("txt-so-bon").value*1;
    var soThuNam = document.getElementById("txt-so-nam").value*1;
    var avg=(soThuNhat+soThuHai+soThuBa+soThuBon+soThuNam)/5
    document.getElementById("trung-binh-cong").innerHTML=`Trung bình cộng là ${avg}`
}



// Cau 3:
function quyDoiTien(){
    event.preventDefault();
    var giaQuyDoi = 23500;
    var nhapSoTien = document.getElementById("txt-tien-do").value*1;
    var tienQuyDoi = (giaQuyDoi*nhapSoTien);
    document.getElementById("tien-do").innerHTML =`Số tiền quy đổi là ${(new Intl.NumberFormat().format(tienQuyDoi))}VND` 
}




// Cau 4:
function tinhToanHoc(){
    event.preventDefault();
    var chieuDai = document.getElementById("txt-chieu-dai").value*1;
    var chieuRong = document.getElementById("txt-chieu-rong").value*1;
    var dienTich = chieuDai*chieuRong;  
    document.getElementById("dien-tich").innerText= `Diện tích hình chữ nhật ${dienTich}`
    var chuVi = (chieuDai+chieuRong)*2;
    document.getElementById("chu-vi").innerText= `Chu vi hình chữ nhật ${chuVi}`

}



// Cau 5:
function tongKySo(){
    event.preventDefault();
    var so = document.getElementById("nhap-so").value*1;
    var so_hang_dv = so%10;
    var so_hang_chuc = so/10;
    var tongHaiKySo =Math.floor(so_hang_dv+so_hang_chuc);
    document.getElementById("ky-so").innerHTML=`Tổng 2 ký số ${tongHaiKySo}`
}